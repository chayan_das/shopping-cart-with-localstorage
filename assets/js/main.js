function validateForm(){
       
		let userName=document.getElementById("user-name").value;
		let email=document.getElementById("email").value;
		let address=document.getElementById("address").value;
		let phone=document.getElementById("phone").value;
		let password=document.getElementById("password").value;
        let confrimPassword=document.getElementById("confirm-passwrod").value;
		let country=document.getElementById("country").value;
		let file=document.getElementById("file").value;


        if(userName.length == 0 || userName == "")
        {   
        	document.getElementById("userError").innerHTML = "Please fill the filed";
        	return false;
        }
        else if(!(/^[a-zA-Z]+$/.test(userName)))
        {
            document.getElementById("userError").innerHTML = "Only alphabets allowed";
            return false;
        }
        else if(!(/^\S{3,}$/.test(userName)))
        {
        	document.getElementById("userError").innerHTML = "Name cannot contain whitespace";
        	return false;
        }
        

        //Validate Email
        else if(email.length == 0 || email == "")
        {   
        	document.getElementById("emailError").innerHTML = "Please fill the filed";
        	return false;
        }
	    else if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))
		{
		    document.getElementById("emailError").innerHTML = "Please Put valid email";
        	return false;
		}
	    
	    // Address Validate
        else if(address.length == 0 || address == "")
        {   
        	document.getElementById("addressError").innerHTML = "Please fill the filed";
        	return false;
        }
        
        // Phone Validate
        else if(phone.length == 0 || phone == "")
        {   
        	document.getElementById("numberError").innerHTML = "Please fill the filed";
        	return false;
        }
        else if(!(/^[0-9]+$/.test(phone)))
        {
        	document.getElementById("numberError").innerHTML = "Plese Put Numeric Number";
        	return false;
        }
        else if(!( /^\d{11}$/.test(phone)))
        {
        	document.getElementById("numberError").innerHTML = "Phone must 11 character";
        	return false;
        }

        // Password Validation
        else if(password.length == 0 || password == "")
        {   
        	document.getElementById("passwordError").innerHTML = "Please fill the filed";
        	return false;
        }
        else if(password <= 6)
        {
        	document.getElementById("passwordError").innerHTML = "Password Minimum 6 Character";
        	return false;
        }
        else if(confrimPassword.length == 0 || confrimPassword == "")
        {   
        	document.getElementById("confirmError").innerHTML = "Please fill the filed";
        	return false;
        }
        else if(password!=confrimPassword)
        {
            document.getElementById("confirmError").innerHTML = "Dont Match";
        	return false;
        }
        
        else if(!males.checked && !females.checked)
        {
            document.getElementById("genderError").innerHTML = "Please Select Your Gender";
            document.getElementById("genderError").style.color="red";
            return false;
        }

        else if(country == 0)
        {
            document.getElementById("country").innerHTML = "Please Select Your Country";
            return false;
        }

        else if(file.length == 0)
        {
        	document.getElementById("fileError").innerHTML = "Please fill the filed";
            return false;
        }
        else if(file >= 2048){
            document.getElementById("fileError").innerHTML = "File too Big, please select a file less than 2mb";
            return false;
        }
        else if(!/(\.pdf|\.exls|\.doc|\.jpg|\.png|\.jpeg)$/i.test(file)) 
	    {
	        document.getElementById("fileError").innerHTML = "You Submit pdf/exls/doc/jpg/png/jpeg Extension";
	    	return false;
	    }
        return true;
	}

    // function remove(){
    //     if(document.getElementById("user-name").length != 0 || document.getElementById("user-name") != ""){
    //         document.getElementById("userError").innerHTML = "";
    //     }
    //     if(email.length != 0 || email != "" ||(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)))
    //     {   
    //         document.getElementById("emailError").innerHTML = "";
    //     }
        
    // }
    // document.getElementById("user-name").addEventListener('blur',remove);
    // document.getElementById("email").addEventListener('blur',remove);
    // document.getElementById("user-name").addEventListener('blur',remove);
    // document.getElementById("user-name").addEventListener('blur',remove);
    // document.getElementById("user-name").addEventListener('blur',remove);



   